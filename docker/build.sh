#!/bin/bash

_CURRENT_DIR=`pwd`
echo ${_CURRENT_DIR}
rm -rf dist

cd ..

echo "\033[44;37mrm -rf node_modules  \033[0m"
rm -rf node_modules
rm -rf dist

echo "\033[44;37mnpm install  \033[0m"
npm install

echo "\033[44;37mnpm run build:prod \033[0m"
npm run build:prod

echo "\033[44;37m cp dist \033[0m"
cp -R dist docker

cd ${_CURRENT_DIR}

echo "\033[44;37m docker build -t registry.cn-beijing.aliyuncs.com/wychuan/eladmin-fe:latest \033[0m"
docker build -t registry.cn-beijing.aliyuncs.com/wychuan/eladmin-fe:latest .

echo "\033[44;37m docker push registry.cn-beijing.aliyuncs.com/wychuan/eladmin-fe:latest \033[0m"

docker login -u docker@1037320446504589 -p Vancl@123 registry.cn-beijing.aliyuncs.com

docker push registry.cn-beijing.aliyuncs.com/wychuan/eladmin-fe:latest
