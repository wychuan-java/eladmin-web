import request from '@/utils/request'

export function getUserStocks(params) {
  return request({
    url: '/api/userstock',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: '/api/userstock',
    method: 'post',
    data
  })
}

export function edit(data) {
  return request({
    url: '/api/userstock',
    method: 'put',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/userstock',
    method: 'delete',
    data: ids
  })
}

export default { getUserStocks, add, edit, del }
