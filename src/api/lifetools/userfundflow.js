import request from '@/utils/request'

export function getUserFundFlows(params) {
  return request({
    url: '/api/userfundflow',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: '/api/userfundflow',
    method: 'post',
    data
  })
}

export function edit(data) {
  return request({
    url: '/api/userfundflow',
    method: 'put',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/userfundflow',
    method: 'delete',
    data: ids
  })
}

export default { getUserFundFlows, add, edit, del }
