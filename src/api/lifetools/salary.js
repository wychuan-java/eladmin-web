import request from '@/utils/request'

export function calculateMonthSalary(data) {
  return request({
    url: 'api/lifetools/salary/month',
    method: 'post',
    data
  })
}

export function calculateYearSalary(data) {
  return request({
    url: 'api/lifetools/salary/year',
    method: 'post',
    data
  })
}

export default { calculateMonthSalary, calculateYearSalary }
