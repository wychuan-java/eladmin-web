import request from '@/utils/request'

export function getUserFunds(params) {
  return request({
    url: '/api/userFund',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: '/api/userfund',
    method: 'post',
    data
  })
}

export function edit(data) {
  return request({
    url: '/api/userfund',
    method: 'put',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/userfund',
    method: 'delete',
    data: ids
  })
}

export default { getUserFunds, add, edit, del }
